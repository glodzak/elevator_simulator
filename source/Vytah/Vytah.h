/**
 * @file    Vytah.h
 * @brief   Application entry point.
 */

#ifndef VYTAH_H_
#define VYTAH_H_

#include <Vytah/Includes.h>
#include <Vytah/UARTConfig.h>

class Vytah {
private:
	PACKET packet;
	uint8_t pohyb;
	uint8_t poschodie;		//2+'0'
	uint8_t elevator_led[5];
	uint8_t cabin_led[5];
	volatile uint8_t vytah_pohyb;
	volatile uint8_t status_cabine;
public:

	Vytah();
	virtual ~Vytah();
	unsigned char get_crc(PACKET *paket);
	unsigned int size_of_data(PACKET *paket);
	void packet_to_send(PACKET *paket);
	uint8_t packet_to_recieve();
	void reset_Watchdog();
	void motor_pohyb(uint8_t smer_pohybu);
	void motor_stop();
	void recieve_command();
	void LED_ZAPNI(uint8_t adresa);
	void LED_VYPNI(uint8_t adresa);
	uint8_t get_Sender_Address();
	uint8_t get_Vytah_Pohyb();
	void DISPLAY();
	void INIT_LED_POSITION();
	void kontrola_tlacidiel();
	void cabine(uint8_t status);
};

extern Vytah *p_vytah;


#endif /* VYTAH_H_ */
