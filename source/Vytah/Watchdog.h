/**
 * @file   Watchdog.h
 * @brief   Application entry point.
 */

#ifndef VYTAH_WATCHDOG_H_
#define VYTAH_WATCHDOG_H_

#include <Vytah/Vytah.h>

#define PIT_IRQ_ID PIT_IRQn
/* Get source clock for PIT driver */
#define PIT_SOURCE_CLOCK CLOCK_GetFreq(kCLOCK_BusClk)




void Watchdog_INIT();
void WATCHDOG_IRQ_HANDLER(void);
#endif /* VYTAH_WATCHDOG_H_ */
