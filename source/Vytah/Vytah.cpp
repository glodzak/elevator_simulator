/**
 * @file    Vytah.cpp
 * @brief   Application entry point.
 */
#include <Vytah/Vytah.h>


UART_Config *_uart;

/**
 * Konštruktor
 */

Vytah::Vytah() {
	// TODO Auto-generated constructor stub
	 pohyb = FLOOR_DOWN;
	 poschodie = 4;
	 vytah_pohyb = FALSE; // výťah stojí
	 cabine(LOCK_THE_DOOR);
	 for(uint8_t i=0;i<5;i++){
		elevator_led[i] = 0;
		cabin_led[i] = 0;
	 }
	 LED_VYPNI(LED_ELEVATOR_P);
	 LED_VYPNI(LED_ELEVATOR_1);
	 LED_VYPNI(LED_ELEVATOR_2);
	 LED_VYPNI(LED_ELEVATOR_3);
	 LED_VYPNI(LED_ELEVATOR_4);

	 LED_VYPNI(LED_CABIN_P);
	 LED_VYPNI(LED_CABIN_1);
	 LED_VYPNI(LED_CABIN_2);
	 LED_VYPNI(LED_CABIN_3);
	 LED_VYPNI(LED_CABIN_4);
}

Vytah::~Vytah() {
	// TODO Auto-generated destructor stub
}
/**
 * Funkcia get pre adresu odosielateľa paketu
 * &return paket.sender_address vráti adresu
 */
uint8_t Vytah::get_Sender_Address() {return packet.sender_address;}

/**
 *	Funkcia get pre pohyb výťahu
 *	@return vytah_pohyb vracia či je výťah v pohybe alebo nie
 */
uint8_t Vytah::get_Vytah_Pohyb() {return vytah_pohyb;}

unsigned int crc_table[256] = {
			0, 94,188,226, 97, 63,221,131,194,156,126, 32,163,253, 31, 65,
			157,195, 33,127,252,162, 64, 30, 95,  1,227,189, 62, 96,130,220,
			 35,125,159,193, 66, 28,254,160,225,191, 93,  3,128,222, 60, 98,
			190,224,  2, 92,223,129, 99, 61,124, 34,192,158, 29, 67,161,255,
			 70, 24,250,164, 39,121,155,197,132,218, 56,102,229,187, 89,  7,
			219,133,103, 57,186,228,  6, 88, 25, 71,165,251,120, 38,196,154,
			101, 59,217,135,  4, 90,184,230,167,249, 27, 69,198,152,122, 36,
			248,166, 68, 26,153,199, 37,123, 58,100,134,216, 91,  5,231,185,
			140,210, 48,110,237,179, 81, 15, 78, 16,242,172, 47,113,147,205,
			 17, 79,173,243,112, 46,204,146,211,141,111, 49,178,236, 14, 80,
			175,241, 19, 77,206,144,114, 44,109, 51,209,143, 12, 82,176,238,
			 50,108,142,208, 83, 13,239,177,240,174, 76, 18,145,207, 45,115,
			202,148,118, 40,171,245, 23, 73,  8, 86,180,234,105, 55,213,139,
			 87,  9,235,181, 54,104,138,212,149,203, 41,119,244,170, 72, 22,
			233,183, 85, 11,136,214, 52,106, 43,117,151,201, 74, 20,246,168,
			116, 42,200,150, 21, 75,169,247,182,232, 10, 84,215,137,107, 53};

/**
 * Funkcia pre odosielanie paketu
 * @param PACKET *paket je smerník na štruktúru, ktorá odosiela paket
 */
void Vytah::packet_to_send(PACKET *paket){
	uint8_t i = 4;							// Ak by som posielal väčší paket -> pretypovať na uint16_t
	uint8_t PACKET_BUFFER[PACKET_SIZE];

	PACKET_BUFFER[0] = paket->start_bit;
	PACKET_BUFFER[1] = paket->reciever_address;
	PACKET_BUFFER[2] = paket->sender_address;
	PACKET_BUFFER[3] = paket->size_of_data;

	while(i < (paket->size_of_data+4)){
		PACKET_BUFFER[i] = paket->data[i-4];
		i++;
	}
	PACKET_BUFFER[i] = paket->CRC8_result;
	paket->actually_size_of_packet = paket->size_of_data + 5;

	for (i=0; i<paket->actually_size_of_packet;i++) {
		PUTCHAR(PACKET_BUFFER[i]);
	}
}

/**
 * Funkcia pre prijatie paketu
 * @return 	0 ak sa nepodarí správne načítať paket z poľa
 * 			1 ak sa paket správne príjme
 */

uint8_t Vytah::packet_to_recieve(){
	//BUFFER_T *cBuffer;
	uint8_t pcBuffer[20];
	uint8_t *buf = &pcBuffer[0];

	if(bufferRead(&outBuffer, buf, 1)){
		if(pcBuffer[0] == packet.start_bit){
			buf++;
			while(!bufferRead(&outBuffer, buf, 1));
			packet.reciever_address = pcBuffer[1];
			buf++;
			while(!bufferRead(&outBuffer, buf, 1));
			packet.sender_address = pcBuffer[2];
			buf++;
			while(!bufferRead(&outBuffer, buf, 1));
			packet.size_of_data = pcBuffer[3];
			buf++;
			for(uint8_t i = 0;i<packet.size_of_data;i++)	{
				while(!bufferRead(&outBuffer,buf+i,1));
				 packet.data[i] = pcBuffer[4+i];
			}
			return 1;
		}
		return 0;
	}
	return 0;
}

/**
 * Funkcia, ktorá vypočíta z dát ich veľkosť pre odoslanie a prijatie paketu
 * @return i vráti veľkosť dát
 */
unsigned int Vytah::size_of_data(PACKET *paket){
	int i = 0;
	while(i < DATA_SIZE){
		if(paket->data[i] == '\0'){
			break;
		}
		else{
			i = i + 1;
		}
	}
	return i;
}

/**
 * Funkcia pre výpočet kontrolného súčtu CRC8
 * &param smerník na štruktúru paket, pre ktorý sa vypočíta tento súčet
 * &return vysledok vráti kontrolný súčet
 */
unsigned char Vytah::get_crc(PACKET *paket)
{
       char vysledok=0;


       vysledok = crc_table[vysledok ^ paket->reciever_address];
       vysledok = crc_table[vysledok ^ paket->sender_address];

       for (uint16_t i = 0; i<paket->size_of_data; i++){
       		vysledok = crc_table[vysledok ^ paket->data[i]];
       }
       return vysledok;
}

/**
 * Funkcia pre reset Watchdog, ktorá po uplynutí časovača periodicky posiela paket
 *
 */

void Vytah::reset_Watchdog(){
	PACKET local_paket;
	local_paket.reciever_address = WATCHDOG;
	local_paket.sender_address = MY_ADDRESS;
	local_paket.data[0] = WATCHDOG_RESET;
	local_paket.size_of_data = size_of_data(&local_paket);
	local_paket.CRC8_result = get_crc(&local_paket);
	packet_to_send(&local_paket);
}

/**
 * Funkcia pre pohyb motora výťahu
 * @param smer_pohybu Určuje smer pohybu výťahu
 */

void Vytah::motor_pohyb(uint8_t smer_pohybu){
	PACKET local_paket;
	if(smer_pohybu == 90){
		local_paket.data[0] = MOTOR_MOVEMENT;
		local_paket.data[1] = 0x00;
		local_paket.data[2] = 0x00;
		local_paket.data[3] = 0x00;
		pohyb = FLOOR_UP;
	}else{
		local_paket.data[0] = MOTOR_MOVEMENT;
		local_paket.data[1] = 0xFF;
		local_paket.data[2] = 0xFF;
		local_paket.data[3] = 0xFF;
		pohyb = FLOOR_DOWN;
	}
	local_paket.data[4] = smer_pohybu;
	local_paket.reciever_address = MOTOR;
	local_paket.sender_address = MY_ADDRESS;
	local_paket.size_of_data = 5;
	local_paket.CRC8_result = get_crc(&local_paket);
	packet_to_send(&local_paket);
	vytah_pohyb = TRUE;
}

/**
 * Funkcia pre zastavenie motora výťahu
 */

void Vytah::motor_stop(){
	PACKET local_paket;
	local_paket.reciever_address = MOTOR;
	local_paket.sender_address = MY_ADDRESS;
	local_paket.data[0] = MOTOR_STOP;
	local_paket.size_of_data = 1;
	local_paket.CRC8_result = get_crc(&local_paket);
	packet_to_send(&local_paket);
}

/**
 * Funkcia pre rozsvietenie LED
 * @param adresa_LED Identifikuje presnú adresu LED, ktorá sa má zasvietiť
 */
void Vytah::LED_ZAPNI(uint8_t adresa_LED){
	uint8_t index_led= (0x0F & adresa_LED); //index LED
	uint8_t type_led = (0xF0 & adresa_LED);
	if(type_led == 0x20){
		cabin_led[index_led] = 1;
	}else{
		elevator_led[index_led] = 1;
	}
	PACKET local_paket;
		local_paket.reciever_address = adresa_LED;	//local_paket.sender_address
		local_paket.sender_address = MY_ADDRESS;
		local_paket.data[0] = LED_ON;
		local_paket.size_of_data = 1;
		local_paket.CRC8_result = get_crc(&local_paket);
		packet_to_send(&local_paket);
}

/**
 * Funkcia pre zhasnutie LED
 * @param adresa_LED Identifikuje presnú adresu LED, ktorá má zhasnúť
 */
void Vytah::LED_VYPNI(uint8_t adresa_LED){
	uint8_t index_led= (0x0F & adresa_LED); //index LED
	uint8_t type_led = (0xF0 & adresa_LED);
	if(type_led == 0x20){
		cabin_led[index_led] = 0;
	}else{
		elevator_led[index_led] = 0;
	}
	PACKET local_paket;
		local_paket.reciever_address =adresa_LED;
		local_paket.sender_address = MY_ADDRESS;
		local_paket.data[0] = LED_OFF;
		local_paket.size_of_data = 1;
		local_paket.CRC8_result = get_crc(&local_paket);
		packet_to_send(&local_paket);
}

/**
 * Funkcia pre rozsvietenie displeja
 */

void Vytah::DISPLAY(){
	PACKET local_paket;
	local_paket.reciever_address = INFORMATION_DISPLAY;
	local_paket.sender_address = MY_ADDRESS;
	local_paket.data[0] = pohyb;
	local_paket.data[1] = poschodie+'0';
	local_paket.size_of_data = 2;
	local_paket.CRC8_result = get_crc(&local_paket);
	packet_to_send(&local_paket);
}

/**
 * Funkcia pre kontrolu stlačených tlačidiel
 */
void Vytah::kontrola_tlacidiel(){
	uint8_t pom_flag = FALSE;
	if(vytah_pohyb == FALSE && status_cabine == LOCK_THE_DOOR){
		for(int i = poschodie-1; i>=0;i--){
			if((elevator_led[i] || cabin_led[i]) && (status_cabine == LOCK_THE_DOOR)){
				motor_pohyb(MOTOR_DOWN);
				pom_flag = TRUE;
				break;
			}
		}
		if(!pom_flag){
			for(int i = poschodie+1; i<=4;i++){
				if((elevator_led[i] || cabin_led[i]) && (status_cabine == LOCK_THE_DOOR)){
					motor_pohyb(MOTOR_UP);
					break;
				}
			}
		}
	}
}

/**
 * Funkcia pre odomknutie a uzamknutie kabíny výťahu
 * @param status slúži pre odomykanie a zamykanie kabíny výťahu
 */

void Vytah::cabine(uint8_t status){
	PACKET local_paket;
	status_cabine = status;
	local_paket.reciever_address = CABIN;
	local_paket.sender_address = MY_ADDRESS;
	local_paket.data[0] = status;
	local_paket.size_of_data = 1;
	local_paket.CRC8_result = get_crc(&local_paket);
	packet_to_send(&local_paket);
}

/**
 * Funkcia pre rozvetvenie prijatého paketu na základe adresy odosielajúceho
 */
void Vytah::recieve_command(){
	switch (packet.sender_address) {
		case BUTTON_CABIN_P:
			if(poschodie == 0 && vytah_pohyb == FALSE){
				cabine(UNLOCK_THE_DOOR);
				PIT_StartTimer(PIT, kPIT_Chnl_1);
			}else{
				LED_ZAPNI(LED_CABIN_P);
			}
			break;
		case BUTTON_CABIN_1:
			if(poschodie == 1 && vytah_pohyb == FALSE){
				cabine(UNLOCK_THE_DOOR);
				PIT_StartTimer(PIT, kPIT_Chnl_1);
			}else{
				LED_ZAPNI(LED_CABIN_1);
			}
			break;
		case BUTTON_CABIN_2:
			if(poschodie == 2 && vytah_pohyb == FALSE){
				cabine(UNLOCK_THE_DOOR);
				PIT_StartTimer(PIT, kPIT_Chnl_1);
			}else{
				LED_ZAPNI(LED_CABIN_2);
			}
			break;
		case BUTTON_CABIN_3:
			if(poschodie == 3 && vytah_pohyb == FALSE){
				cabine(UNLOCK_THE_DOOR);
				PIT_StartTimer(PIT, kPIT_Chnl_1);
			}else{
				LED_ZAPNI(LED_CABIN_3);
			}
			break;
		case BUTTON_CABIN_4:
			if(poschodie == 4 && vytah_pohyb == FALSE){
				cabine(UNLOCK_THE_DOOR);
				PIT_StartTimer(PIT, kPIT_Chnl_1);
			}else{
				LED_ZAPNI(LED_CABIN_4);
			}
			break;
		case BUTTON_ELEVATOR_P:
			if(poschodie == 0 && vytah_pohyb == FALSE){
				cabine(UNLOCK_THE_DOOR);
				PIT_StartTimer(PIT, kPIT_Chnl_1);
			}else{
				LED_ZAPNI(LED_ELEVATOR_P);
			}
			break;
		case BUTTON_ELEVATOR_1:
			if(poschodie == 1 && vytah_pohyb == FALSE){
				cabine(UNLOCK_THE_DOOR);
				PIT_StartTimer(PIT, kPIT_Chnl_1);
			}else{
				LED_ZAPNI(LED_ELEVATOR_1);
			}
			break;
		case BUTTON_ELEVATOR_2:
			if(poschodie == 2 && vytah_pohyb == FALSE){
				cabine(UNLOCK_THE_DOOR);
				PIT_StartTimer(PIT, kPIT_Chnl_1);
			}else{
				LED_ZAPNI(LED_ELEVATOR_2);
			}
			break;
		case BUTTON_ELEVATOR_3:
			if(poschodie == 3 && vytah_pohyb == FALSE){
				cabine(UNLOCK_THE_DOOR);
				PIT_StartTimer(PIT, kPIT_Chnl_1);
			}else{
				LED_ZAPNI(LED_ELEVATOR_3);
			}
			break;
		case BUTTON_ELEVATOR_4:
			if(poschodie == 4 && vytah_pohyb == FALSE){
				cabine(UNLOCK_THE_DOOR);
				PIT_StartTimer(PIT, kPIT_Chnl_1);
			}else{
				LED_ZAPNI(LED_ELEVATOR_4);
			}
			break;
		case LIMIT_SWITCH_P:
			if(packet.data[0] == CABIN_IN_NARROW_PROXIMITY){	// cervena farba pre limit switch
				poschodie = 0;
				DISPLAY();
				if(status_cabine == LOCK_THE_DOOR && (elevator_led[0] || cabin_led[0])){
					cabine(UNLOCK_THE_DOOR);
					PIT_StartTimer(PIT, kPIT_Chnl_1);
					LED_VYPNI(LED_ELEVATOR_P);
					LED_VYPNI(LED_CABIN_P);
					vytah_pohyb = FALSE;
				}
			}else if(packet.data[0] == CABIN_IN_WIDE_PROXIMITY && (elevator_led[0] || cabin_led[0])){	// oranzova farba pre limit switch
				motor_stop();
			}
			break;
		case LIMIT_SWITCH_1:
			if(packet.data[0] == CABIN_IN_NARROW_PROXIMITY){
				poschodie = 1;
				DISPLAY();
				if(status_cabine == LOCK_THE_DOOR && (elevator_led[1] || cabin_led[1])){
					cabine(UNLOCK_THE_DOOR);
					PIT_StartTimer(PIT, kPIT_Chnl_1);
					LED_VYPNI(LED_ELEVATOR_1);
					LED_VYPNI(LED_CABIN_1);
					vytah_pohyb = FALSE;
				}
			}else if(packet.data[0] == CABIN_IN_WIDE_PROXIMITY && (elevator_led[1] || cabin_led[1])){
				motor_stop();
			}
			break;
		case LIMIT_SWITCH_2:
			if(packet.data[0] == CABIN_IN_NARROW_PROXIMITY){
				poschodie = 2;
				DISPLAY();
				if(status_cabine == LOCK_THE_DOOR && (elevator_led[2] || cabin_led[2])){
					cabine(UNLOCK_THE_DOOR);
					PIT_StartTimer(PIT, kPIT_Chnl_1);
					LED_VYPNI(LED_ELEVATOR_2);
					LED_VYPNI(LED_CABIN_2);
					vytah_pohyb = FALSE;
				}
			}else if(packet.data[0] == CABIN_IN_WIDE_PROXIMITY && (elevator_led[2] || cabin_led[2])){
				motor_stop();
			}
			break;
		case LIMIT_SWITCH_3:
			if(packet.data[0] == CABIN_IN_NARROW_PROXIMITY){
				poschodie = 3;
				DISPLAY();
				if(status_cabine == LOCK_THE_DOOR && (elevator_led[3] || cabin_led[3])){
					cabine(UNLOCK_THE_DOOR);
					PIT_StartTimer(PIT, kPIT_Chnl_1);
					LED_VYPNI(LED_ELEVATOR_3);
					LED_VYPNI(LED_CABIN_3);
					vytah_pohyb = FALSE;
				}
			}else if((packet.data[0] == CABIN_IN_WIDE_PROXIMITY) && (elevator_led[3] || cabin_led[3])){
				motor_stop();
			}
			break;
		case LIMIT_SWITCH_4:
			if(packet.data[0] == CABIN_IN_NARROW_PROXIMITY){
				poschodie = 4;
				DISPLAY();
				if(status_cabine == LOCK_THE_DOOR && (elevator_led[4] || cabin_led[4])){
					cabine(UNLOCK_THE_DOOR);
					PIT_StartTimer(PIT, kPIT_Chnl_1);
					LED_VYPNI(LED_ELEVATOR_4);
					LED_VYPNI(LED_CABIN_4);
					vytah_pohyb = FALSE;
				}
			}else if(packet.data[0] == CABIN_IN_WIDE_PROXIMITY && (elevator_led[4] || cabin_led[4])){
				motor_stop();
			}
			break;
	}
}



