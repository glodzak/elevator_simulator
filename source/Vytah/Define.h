/**
 * @file    Define.h
 * @brief   Application entry point.
 */

#ifndef VYTAH_DEFINE_H_
#define VYTAH_DEFINE_H_

#define DATA_SIZE 256
#define PACKET_SIZE 261
#define MY_ADDRESS 0xFF

#define BUTTON_CABIN_P 0xB0			/// tlačidlá kabína
#define BUTTON_CABIN_1 0xB1
#define BUTTON_CABIN_2 0xB2
#define BUTTON_CABIN_3 0xB3
#define BUTTON_CABIN_4 0xB4
#define BUTTON_ELEVATOR_P 0xC0			/// tlačidlá výťah
#define BUTTON_ELEVATOR_1 0xC1
#define BUTTON_ELEVATOR_2 0xC2
#define BUTTON_ELEVATOR_3 0xC3
#define BUTTON_ELEVATOR_4 0xC4

#define CABIN 0xF0					/// definícia kabíny
#define UNLOCK_THE_DOOR 0x00
#define LOCK_THE_DOOR 0x01

#define EMERGENCY_BREAK 0xF			/// definícia záchranná brzda
#define DEACTIVATE_BREAK 0x00
#define ACTIVATE_BREAK 0x01

#define INFORMATION_DISPLAY 0x30	/// definícia informačný displej k poschodiam
#define FLOOR_UP 0x01
#define FLOOR_DOWN 0x02

#define LED_ON 0x01					/// LED ON, OFF define
#define LED_OFF 0x00
#define LED_ELEVATOR_P 0x10			/// definície LED na poschodia výťah
#define LED_ELEVATOR_1 0x11
#define LED_ELEVATOR_2 0x12
#define LED_ELEVATOR_3 0x13
#define LED_ELEVATOR_4 0x14

#define LED_CABIN_P 0x20			/// definície LED v kabíne
#define LED_CABIN_1 0x21
#define LED_CABIN_2 0x22
#define LED_CABIN_3 0x23
#define LED_CABIN_4 0x24

#define SWITCH_LOW 0x00						/// SWITCH definície
#define CABIN_IN_WIDE_PROXIMITY 0x01		/// oranžov
#define CABIN_IN_NARROW_PROXIMITY 0x02		/// červená
#define LIMIT_SWITCH_P 0xE0					/// Prepínače na poschodiach
#define LIMIT_SWITCH_1 0xE1
#define LIMIT_SWITCH_2 0xE2
#define LIMIT_SWITCH_3 0xE3
#define LIMIT_SWITCH_4 0xE4

#define MOTOR 0xF1					/// MOTOR
#define MOTOR_STOP 0x01
#define MOTOR_MOVEMENT 0x02
#define MOTOR_ENCODER_COUNT 0x03
#define MOTOR_UP 90
#define MOTOR_DOWN ~(90-1)

#define TERMINAL 0xd0				// Terminal
#define WATCHDOG 0xFE				// Watchdog časovač
#define WATCHDOG_RESET 0x02

#define START_BIT 0xA0				/// START BIT v pakete
#define TRUE 1
#define FALSE 0

#define BUFFER_SIZE 20

#define BUTTON_DATA_SIZE 0x06

typedef struct packet {					/// GLOBÁLNA premenná paket
	uint8_t start_bit = START_BIT;
	uint8_t reciever_address;
	uint8_t sender_address;
	uint8_t size_of_data;
	char data[DATA_SIZE];
	char CRC8_result;
	uint8_t actually_size_of_packet;
}PACKET;

#endif /** VYTAH_DEFINE_H_ */
