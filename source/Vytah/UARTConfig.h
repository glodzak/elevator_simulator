/**
 * @file    UARTConfig.h
 * @brief   Application entry point.
 */

#ifndef VYTAH_UART_CONFIG_UARTCONFIG_H_
#define VYTAH_UART_CONFIG_UARTCONFIG_H_

#include "Vytah/utils/CircularBuffer.h"
#define _bufferUartSize 20

extern BUFFER_T cBuffer;
extern BUFFER_T outBuffer;

class UART_Config {
public:
	UART_Config();
	virtual ~UART_Config();
	void CBufferUart_Init();
private:
	int __sys_write(int iFileHandle, char *pcBuffer, int iLength);
};


#endif /** VYTAH_UART_CONFIG_UARTCONFIG_H_ */

