/**
 * @file    UARTConfig.cpp
 * @brief   Application entry point.
 */

#include <Vytah/UARTConfig.h>
#include <Vytah/Vytah.h>

Vytah *vytah;
BUFFER_T cBuffer;
BUFFER_T outBuffer;
lpsci_config_t user_config;
uint8_t OUTPUT_BUFFER[BUFFER_SIZE];
uint8_t RECIEVED_BUFFER[BUFFER_SIZE];

UART_Config::UART_Config() {
	// TODO Auto-generated constructor stub

}

UART_Config::~UART_Config() {
	// TODO Auto-generated destructor stub
}
extern "C" void UART0_IRQHandler(void) {
	UART0_Type *base = UART0;
	uint8_t pcBuffer;

	if ((base->S1 & kLPSCI_TxDataRegEmptyFlag)) {
		int c = bufferRead(&cBuffer, &pcBuffer, 1);
		if (c > 0) {
			UART0->D = pcBuffer;
		}else{
		/// Disable TX register empty interrupt.
			LPSCI_DisableInterrupts(base, kLPSCI_TxDataRegEmptyInterruptEnable);
		}
		LPSCI_ClearStatusFlags(base, kLPSCI_TxDataRegEmptyFlag);
	}
	///  If RX overrun.
	if (UART0_S1_OR_MASK & base->S1){
		while (UART0_S1_RDRF_MASK & base->S1){
			(void)base->D;
	    }
	    LPSCI_ClearStatusFlags(base, kLPSCI_RxOverrunFlag);
	}

	/// Receive data register full
	if ((UART0_S1_RDRF_MASK & base->S1) && (UART0_C2_RIE_MASK & base->C2)){
		uint8_t rxData;
		static uint8_t size = 0;
		rxData = base->D;

		bufferWrite(&outBuffer, &rxData, 1);
	}
}

/// Redefine __sys_write to handle printf output
int UART_Config::__sys_write(int iFileHandle, char *pcBuffer, int iLength) {
	/// Write String to BUFFER
	int size = bufferWrite(&cBuffer, (uint8_t *)pcBuffer, iLength);
	LPSCI_EnableInterrupts(UART0, kLPSCI_TxDataRegEmptyInterruptEnable | kLPSCI_RxOverrunInterruptEnable);
	return iLength - size;

}

void UART_Config::CBufferUart_Init() {
	bufferInit(&cBuffer, RECIEVED_BUFFER, sizeof(RECIEVED_BUFFER));
	bufferInit(&outBuffer,OUTPUT_BUFFER, sizeof(OUTPUT_BUFFER));

	LPSCI_GetDefaultConfig(&user_config);
	user_config.baudRate_Bps = 57600U;
	user_config.enableTx = true;
	user_config.enableRx = true;

	LPSCI_Init(UART0, &user_config, CLOCK_GetFreq(kCLOCK_PllFllSelClk));
	LPSCI_DisableInterrupts(UART0, kLPSCI_AllInterruptsEnable);

	/** Enable LPSCI RX IRQ if previously enabled. */
	LPSCI_EnableInterrupts(UART0, kLPSCI_RxDataRegFullInterruptEnable);

	/** Enable interrupt in NVIC. */
	EnableIRQ(UART0_IRQn);

}

