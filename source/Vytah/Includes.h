/**
 * @file    Includes.h
 * @brief   Application entry point.
 */

#ifndef INSTRUCTION_SET_H_
#define INSTRUCTION_SET_H_

/// Includes

#include "board.h"
#include <stdio.h>
#include <string.h>
#include <fsl_clock.h>
#include <fsl_lpsci.h>
#include "fsl_pit.h"
#include "board.h"
#include "peripherals.h"
#include "pin_mux.h"
#include "clock_config.h"
#include "MKL25Z4.h"
#include "fsl_debug_console.h"
#include <assert.h>
#include <stddef.h>
#include <stdint.h>
#include <ctype.h>
#include "Vytah/Define.h"


/// Variables

extern uint8_t OUTPUT_BUFFER[BUFFER_SIZE];
extern uint8_t RECIEVED_BUFFER[BUFFER_SIZE];


#endif /** INSTRUCTION_SET_H_ */
