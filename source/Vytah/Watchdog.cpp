/**
 * @file    Watchdog.cpp
 * @brief   Application entry point.
 */
#include "Vytah/Watchdog.h"

/**
 *  Obslužná rutina pre PIT prerušenie
 */
extern "C" void PIT_IRQHandler(void){


    if(PIT->CHANNEL[kPIT_Chnl_0].TFLG){										// bolo vyvolne prerusenie na channel 0?
    	 PIT_ClearStatusFlags(PIT, kPIT_Chnl_0, kPIT_TimerFlag); 			/* Clear interrupt flag.*/
    	p_vytah->reset_Watchdog();
    }else if (PIT->CHANNEL[kPIT_Chnl_1].TFLG) {
    	PIT_ClearStatusFlags(PIT, kPIT_Chnl_1, kPIT_TimerFlag); 			/* Clear interrupt flag.*/
    	p_vytah->cabine(LOCK_THE_DOOR);
    	PIT_StopTimer(PIT, kPIT_Chnl_1);
    }

/**
 *  Inicializácia prerušenia PIT pre Watchdog a kabínu
 */
}

void Watchdog_INIT(){
	pit_config_t pitConfig;
    PIT_GetDefaultConfig(&pitConfig);
    PIT_Init(PIT, &pitConfig);			/* Init pit module */

    PIT_SetTimerPeriod(PIT, kPIT_Chnl_0, USEC_TO_COUNT(700000U, PIT_SOURCE_CLOCK));	/* Set timer period for channel 0 */
    PIT_EnableInterrupts(PIT, kPIT_Chnl_0, kPIT_TimerInterruptEnable);		/* Enable timer interrupts for channel 0 */

    PIT_SetTimerPeriod(PIT, kPIT_Chnl_1, USEC_TO_COUNT(3500000U, PIT_SOURCE_CLOCK));	/* Set timer period for channel 0 */
    PIT_EnableInterrupts(PIT, kPIT_Chnl_1, kPIT_TimerInterruptEnable);		/* Enable timer interrupts for channel 0 */

    EnableIRQ(PIT_IRQ_ID);				/* Enable at the NVIC */
    PIT_StartTimer(PIT, kPIT_Chnl_0);
}


