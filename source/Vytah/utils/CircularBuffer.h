/**
 * @file    CircularBuffer.h
 * @brief   Application entry point.
 */

#ifndef CIRCULARBUFFER_H_
#define CIRCULARBUFFER_H_

#include "Vytah/Includes.h"
/**
 * Enumerator
 *
 */
enum {
	BUFF_EMPTY, BUFF_FREE, BUFF_FULL
};

/**
 * struct buffer_t
 */

typedef struct buffer_t{
	uint8_t *buf;
	uint8_t *head, *tail;
	size_t size;
	uint8_t full;
} BUFFER_T;

int16_t bufferWrite(BUFFER_T* buff, uint8_t* element, uint16_t count);
int16_t bufferRead(BUFFER_T* buff, uint8_t* element, uint16_t count);
void bufferInit(BUFFER_T* buff, uint8_t* bufferPt, uint16_t size);
size_t bufferCapacity(BUFFER_T* buff);

size_t bufferBytesFree(const BUFFER_T *rb);

#endif /* CIRCULARBUFFER_H_ */
